import math

sum = 0
imax = 100
jmax = imax
kmax = imax
dx = 0.01
dy = dx
dz = dx

for i in range(imax):
    i += 1
    for j in range(jmax):
	    j += 1
	    for k in range(kmax):
		    k += 1
		    sum = sum + math.sin(k*j*i*dz*dy*dx)*dz*dy*dz
print(sum)