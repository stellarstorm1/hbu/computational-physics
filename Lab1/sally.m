pkg load symbolic

sum=0;
dx=0.01;
dy=0.01;
dz=0.01;
imax=100;
jmax=100;
kmax=100;

syms x;
syms y;
syms z;
# eval_ans = eval(int(int(int(sin(x*y*z), 0, 1), 0, 1), 0, 1))

for i=1:imax
  for j=1:jmax
    for k=1:kmax
      sum = sum + sin(j*i*k*dz*dy*dx)*dz*dy*dy;
    endfor
  endfor
endfor


sum