clear all

N = 101;
A = zeros(N, N);
steps = 1000;

top_sum = 0;
right_sum = 0;
bot_sum = 0;
left_sum = 0;

% set border
A(1:N, 1) = 1;
A(1:N, N) = 1;
A(1, 1:N) = 1;
A(N, 1:N) = 1;

for n = 1:steps
  i = 51;
  j = 51;
  while A(i, j) != 1
    direction = rand;
    if direction < 0.25
      i = i - 1;
    elseif direction < 0.5
      i = i + 1;
    elseif direction < 0.75
      j = j - 1;
    else
      j = j + 1;
    endif
  endwhile
  % in this case, potential is non-zero only on the top boundary
  if j == 1
    top_sum = top_sum + 1;
  endif
endfor

avg_V = top_sum / steps;
fprintf('Average potential at the center is %f\n', avg_V);