import matplotlib.pyplot as plt
import numpy as np
total_iterations = 100

walkers = 1000

run_vals = np.zeros(total_iterations)

top_V = 1
bot_V = 0
right_V = 0
left_V = 0

for k in range(total_iterations):
    N = (k + 1) * 100
    A = np.zeros(shape=(N, N))
    A[0:N - 1, 0] = 1
    A[0:N - 1, -1] = 1
    A[0, 0:N - 1] = 1
    A[-1, 0:N - 1] = 1
    top_sum = 0
    right_sum = 0
    bot_sum = 0
    left_sum = 0

    for n in range(walkers):
        i = int(N / 2)
        j = int(N / 2)
        while A[i, j] != 1:
            direction = np.random.rand()
            if direction < 0.25:
                i = i - 1
            elif direction < 0.5:
                i = i + 1
            elif direction < 0.75:
                j = j - 1
            else:
                j = j + 1

        if i == 0:
            # If corner, then half contribution from both sides
            if j == 0:
                top_sum += top_V / 2
                left_sum += left_V / 2
            elif j == N - 1:
                top_sum += top_V / 2
                right_sum += right_V / 2
            else:
                top_sum += top_V
        elif i == N - 1:
            # If corner, then half contribution from both sides
            if j == 0:
                bot_sum += bot_V / 2
                left_sum += left_V / 2
            elif j == N - 1:
                bot_sum += bot_V / 2
                right_sum += right_V / 2
            else:
                bot_sum += bot_V
        elif j == 0 and all([i != 0, i != N - 1]):
            left_sum += left_V
        elif j == N - 1 and all([i != 0, i != N - 1]):
            right_sum += right_V

    avg_V = (top_sum + bot_sum + right_sum + left_sum) / walkers
    run_vals[k] = avg_V
    print(k, avg_V)

np.save('potentials.npy', run_vals)

plt.plot(run_vals, 'bo')
plt.show()
