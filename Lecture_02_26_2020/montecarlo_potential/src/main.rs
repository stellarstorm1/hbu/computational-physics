extern crate ndarray;
extern crate ndarray_npy;
extern crate rand;

use ndarray::prelude::*;
use ndarray_npy::write_npy;
use rand::prelude::*;

fn main() {
    let total_iterations = 100;
    let walkers = 1_000;
    let mut run_vals = Array::<f64, _>::zeros(total_iterations);

    let top_v: f64 = 1.0;
    let bot_v: f64 = 0.0;
    let right_v: f64 = 0.0;
    let left_v: f64 = 0.0;

    for k in 0..total_iterations {
        let n = (k + 1) * 100;
        let mut a = Array::<f64, _>::zeros((n, n));
        a.row_mut(0).fill(1.0);
        a.row_mut(n - 1).fill(1.0);
        a.column_mut(0).fill(1.0);
        a.column_mut(n - 1).fill(1.0);
        let mut top_sum: f64 = 0.0;
        let mut right_sum: f64 = 0.0;
        let mut bot_sum: f64 = 0.0;
        let mut left_sum: f64 = 0.0;

        for _n in 0..walkers {
            let mut i = n / 2;
            let mut j = n / 2;

            while a[[i, j]] != 1.0 {
                let mut rng = rand::thread_rng();
                let direction: f64 = rng.gen();
                if direction < 0.25 {
                    i = i - 1;
                } else if direction < 0.5 {
                    i = i + 1;
                } else if direction < 0.75 {
                    j = j - 1;
                } else {
                    j = j + 1
                }
            }

            if i == 0 {
                // If corner, then half contribution from both sides
                if j == 0 {
                    top_sum = top_sum + (top_v / 2.0);
                    left_sum = left_sum + (left_v / 2.0);
                } else if j == n - 1 {
                    top_sum = top_sum + (top_v / 2.0);
                    right_sum = right_sum + (right_v / 2.0);
                } else {
                    top_sum = top_sum + top_v;
                }
            } else if i == n - 1 {
                if j == 0 {
                    bot_sum = bot_sum + (bot_v / 2.0);
                    left_sum = left_sum + (left_v / 2.0);
                } else if j == n - 1 {
                    bot_sum = bot_sum + (bot_v / 2.0);
                    right_sum = right_sum + (right_sum / 2.0);
                } else {
                    bot_sum = bot_sum + bot_v;
                }
            } else if j == 0 && i != 0 && i != n - 1 {
                left_sum = left_sum + left_v;
            } else if j == n - 1 && i != 0 && i != n - 1 {
                right_sum = right_sum + right_v;
            }
        }
        let avg_v = (top_sum + bot_sum + right_sum + left_sum) / walkers as f64;
        run_vals[k] = avg_v;
        println!("{}, {}", k, avg_v);
    }
    write_npy("mc_potentials.npy", run_vals).expect("Failed to write file");
}
