% Lab 3 - logistic equation

clear all;

r = 3.8;
imax = 25;
x = zeros(imax, 1);
x1 = zeros(imax, 1);
t = zeros(imax, 1);
x(1) = 0.1;
x1(1) = 0.1001;
t(1) = 0.0;

for i = 1:imax - 1
    t(i + 1) = i;
    x(i + 1) = r * x(i) * (1 - x(i));
    x1(i + 1) = r * x1(i) * (1 - x1(i));
endfor

plot(t, x, 'r', "linewidth", 5, t, x1, 'b', "linewidth", 5)
