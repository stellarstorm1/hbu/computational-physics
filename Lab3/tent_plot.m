% Lab 3 - Tent Plot
clear all
bet = 0.7;
imax = 25;

x = zeros(imax, 1);
x1 = zeros(imax, 1);
t = zeros(imax, 1);
x(1) = 0.1;
x1(1) = 0.1001;
t(1) = 0.0;

for i = 1:imax - 1
    t(i + 1) = i;

    if (0 < x(i) && x(i) < 0.5)
        x(i + 1) = 2 * bet * x(i);
        x1(i + 1) = 2 * bet * x1(i);
    else
        x(i + 1) = bet * x(i) * (1 - x(i));
        x1(i + 1) = bet * x1(i) * (1 - x1(i));
    end

end

plot(t, x, 'r', "linewidth", 5, t, x1, 'b', "linewidth", 5)
