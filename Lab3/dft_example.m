% Lab 3 - discrete Fourier transform (DFT)
clear all

dt = 0.001;
tmax = 2.0;
t = 0:dt:tmax;
A = 2 * sin(2 * pi * 150.0 * t) + 5 * sin(2 * pi * 333.0 * t);

nmax = length(t);

fmin = 300;
fmax = 350;
mmax = 1000;

Ftx = zeros(1, nmax);

f = linspace(fmin, fmax, mmax);

for m = 1:mmax
  for n = 1:nmax
    Ftx(m) = Ftx(m) + A(n) * exp(-1i*2*pi*f(m)*n*dt);
  endfor
endfor

Ftx = Ftx/ (nmax/2);
% All values from 1001 onwards are 0+0i in Octave and have to be dropped for
% plotting
Ftx = Ftx(1:1000);

plot(f, abs(Ftx), 'r', 'linewidth', 5)

set(gca, 'XTick', fmin:3:fmax, 'fontsize', 20)
xlabel('frequency')
ylabel('Fourier transform')
