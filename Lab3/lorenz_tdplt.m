% Lab 3 - Time Delay Plot of Lorenz Model
% specify parameters and time step

rho = 28;
sigma = 10;
beta = 8/3;
dt = 0.01;

% create function handles

f = @(x, y, z)sigma * (y - x);
g = @(x, y, z)rho * x - y - x * z;
h = @(x, y, z)x * y - beta * z;

% specify number of time steps and create arrays

nmax = 5000;

x = zeros(nmax, 1);
y = zeros(nmax, 1);
z = zeros(nmax, 1);
t = zeros(nmax, 1);

% arrays for time delay coordinates

x1 = zeros(nmax, 1);
x2 = zeros(nmax, 1);
x3 = zeros(nmax, 1);

% specify initial conditions

x(1) = 0.1; y(1) = 0.1; z(1) = 0.1; t(1) = 0.0;

% integrate the Lorenz system using the 2nd -order algorithm

for n = 1:nmax
    k1x = dt * f(x(n), y(n), z(n));
    k2x = dt * f(x(n) + k1x / 2, y(n), z(n));

    k1y = dt * g(x(n), y(n), z(n));
    k2y = dt * g(x(n), y(n) + k1y / 2, z(n));

    k1z = dt * h(x(n), y(n), z(n));
    k2z = dt * h(x(n), y(n), z(n) + k1z / 2);

    x(n + 1) = x(n) + k2x;

    y(n + 1) = y(n) + k2y;

    z(n + 1) = z(n) + k2z;

    t(n + 1) = t(n) + dt;
end

% two time delays of the x-time series

for n = 1:nmax - 10
    x1(n) = x(n);
    x2(n) = x(n + 5);
    x3(n) = x(n + 10);
end

% create a 3-D time delay plot

plot3(x1, x2, x3, 'k')
xlabel('x(t)')
ylabel('x(t+tau)')
zlabel('x(t+2 tau)')
