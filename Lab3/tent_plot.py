# Lab 3 - Tent Plot
import matplotlib.pyplot as plt
import numpy as np

beta = 0.7
imax = 25

x = np.zeros(imax)
x1 = np.zeros(imax)
t = np.zeros(imax)
x[0] = 0.1
x1[0] = 0.1001
t[0] = 0.0

for i in range(imax - 1):
    t[i + 1] = i
    if 0 < x[i] < 0.5:
        x[i + 1] = 2 * beta * x[i]
        x1[i + 1] = 2 * beta * x1[i]
    else:
        x[i + 1] = beta * x[i] * (1 - x[i])
        x1[i + 1] = beta * x1[i] * (1 - x1[i])

plt.plot(t, x, 'r')
plt.plot(t, x1, 'b')
plt.show()
