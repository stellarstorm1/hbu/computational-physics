% Lab 3 - Single-sided FFT

dt = 0.001;
tmax = 2.0;
t = 0:dt:tmax;
A = 2 * sin(2 * pi * 150.0 * t) + 5 * sin(2 * pi * 333.0 * t);
y = fft(A);
fmax = 1/(2.0*dt);
f = linspace(0, fmax, length(t)/2);
m = abs(y) / length(f);
m = m(1:length(f));
plot(f, m)
xlabel('frequency')
ylabel('abs(FFT)')