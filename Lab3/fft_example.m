% Lab 3 - fft example
dt = 0.001;
tmax = 2.0;
t = 0:dt:tmax;
A = 2 * sin(2 * pi * 150.0 * t) + 5 * sin(2 * pi * 333.0 * t);
y = fft(A);
plot(abs(y))
xlabel('index')
ylabel('abs(FFT)')
