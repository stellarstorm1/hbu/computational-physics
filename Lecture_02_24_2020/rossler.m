% Lecture 02/24/2020 - Rossler Attractor

function yp=rossler(t, y)
  
  global a b c
  yp = [-y(2) - y(3);
        y(1) + a * y(2);
        b + y(3) * (y(1) - c)];
endfunction
