"""
Lecture 02/22/2020 - Bifurcation Plot
"""
import matplotlib.pyplot as plt
import numpy as np

# Tested up to mmax = 201 and nmax = 10000
mmax = 200
nmax = 100
x = np.full(shape=mmax * nmax, fill_value=np.nan)
mu = np.full(shape=mmax * nmax, fill_value=np.nan)

idx = 0
for i in range(mmax):
    temp_mu = 2 + 0.01 * i
    temp_x = 0.2
    for j in range(nmax):
        temp_x = temp_mu * temp_x * (1 - temp_x)
        # Toss out initial values to allow the system to stabilize
        if j > 20:
            x[idx] = temp_x
            mu[idx] = temp_mu
        idx += 1

plt.scatter(mu, x, s=1.5, c='k', marker='.')
plt.show()
