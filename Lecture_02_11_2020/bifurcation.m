% Lecture 02/11/2020 - Bifurcation Diagram
figure
hold on
for i = 1:200
  mu = 2 + 0.01 * i;
  x = 0.2;
  for j = 1:100
    x = mu * x * (1-x);
    if j > 20
      plot(mu, x, '.k')
    endif
  endfor
endfor
