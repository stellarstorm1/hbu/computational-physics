% Lab 6 - example of separation of variables
clear all

% set up a 20x20 spatial grid
imax = 20;
jmax = 20;

% set the upper limit of the sum
nmax = 201;

% initialize the S array
S = zeros(imax, jmax);

% spatial loop over the grid
for i = 1:imax
  for j = 1:jmax
    x = (i - 1) / (imax - 1);
    y = (j - 1) / (jmax - 1);
    
    % spatial loop over the grid
    for n = 1:2:nmax
      G1 = 4 * sin(n * pi * x) / (n * pi);
      G2 = cosh(n * pi * y) - sinh(n * pi * y) * (cosh(n * pi) / sinh(n * pi));
      G3 = 4 * sin(n * pi * y) / (n * pi);
      G4 = cosh(n * pi * x) - sinh(n * pi * x) * (cosh(n * pi) / sinh(n * pi));
      S(i, j) = S(i, j) + G1*G2 + G3*G4;
    endfor
  endfor
endfor

% prevent "dip" in mesh/contour
S(1, 1) = 1;

% create a mesh plot of the solution
mesh(S);