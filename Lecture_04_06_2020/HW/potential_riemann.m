% Assessment 04/06/2020
% Riemann sum estimation for potential from charge density
clear all;

% For simplicity just set charge density equal to inverse Coulomb's constant
ke = 9*10^9;
rho = 1/ke;

dx = 0.01;
dy = 0.01;
dz = 0.01;

% differential * respective max should equal 1
imax = 1/dx;
jmax = 1/dy;
kmax = 1/dz;

% position of the point in space - my choice
x = 9;
y = 5;
z = 3;

denom=@(xprime, yprime, zprime) sqrt((x-xprime)^2 + (y-yprime)^2 + (z-zprime)^2);

sum = 0;

for k = 1:kmax
  for j = 1:jmax
    for i = 1:imax
      sum = sum + ke*rho*(1/denom(i*dx, j*dy, k*dz))*dx*dy*dz;
    endfor
  endfor
endfor

fprintf('Potential is %f\n', sum)
