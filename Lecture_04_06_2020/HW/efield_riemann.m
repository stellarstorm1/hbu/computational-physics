% Assessment 04/06/2020
% Riemann sum estimation for electric field from charge density
clear all;

% For simplicity just set charge density equal to inverse Coulomb's constant
ke = 9*10^9;
rho = 1/ke;

dx = 0.01;
dy = 0.01;
dz = 0.01;

% differential * respective max should equal 1
imax = 1/dx;
jmax = 1/dy;
kmax = 1/dz;

% position of the point in space - my choice
x = 9;
y = 5;
z = 3;

denom=@(xprime, yprime, zprime) ((x-xprime)^2 + (y-yprime)^2 + (z-zprime)^2)^(3/2);

x_sum = 0;
y_sum = 0;
z_sum = 0;

for k = 1:kmax
  for j = 1:jmax
    for i = 1:imax
      x_sum = x_sum + ke*rho*(1/denom(i*dx, j*dy, k*dz))*(x - i*dx)*dx*dy*dz;
      y_sum = y_sum + ke*rho*(1/denom(i*dx, j*dy, k*dz))*(y - j*dy)*dx*dy*dz;
      z_sum = z_sum + ke*rho*(1/denom(i*dx, j*dy, k*dz))*(z - k*dz)*dx*dy*dz;
    endfor
  endfor
endfor

r_sum = sqrt(x_sum^2 + y_sum^2 + z_sum^2);

fprintf('E_x is %f\n', x_sum)
fprintf('E_y is %f\n', y_sum)
fprintf('E_z is %f\n', z_sum)
fprintf('E is %f\n', r_sum)
