% Lab 5 - Basin map of Julia set
clear all
nmax = 10;
x = zeros(nmax, 1);
y = zeros(nmax, 1);

mmax = 100;

J = zeros(mmax, mmax);

x0 = linspace(-1.5, 1.5, mmax);
y0 = linspace(-1.5, 1.5, mmax);

const = -0.75 + i * 0.02;

for i = 1:mmax
  for j = 1:mmax
    x(1) = x0(i);
    y(1) = y0(j);
    for n = 1:nmax - 1
      y(n + 1) = 2 * x(n) * y(n) + imag(const);
      x(n + 1) = x(n)^2 - y(n)^2 + real(const);
    endfor
    if sqrt(x(n)^2 + y(n)^2) < 5.0
      J(i, j) = 1;
    endif
  endfor
endfor

contourf(J)

axis equal
colormap gray
title('Julia Set')
