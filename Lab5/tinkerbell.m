% Lab 5 - Basin boundary of the Tinkerbell map
clear all
nmax = 10;  % 50 in assignment but overflows
x = zeros(nmax, 1);
y = zeros(nmax, 1);

a = 0.9;
b = -0.6013;
c = 2.0;
d = 0.50;

mmax = 300;

M = zeros(mmax, mmax);

x0 = linspace(-2.0, 2.0, mmax);
y0 = linspace(-2.0, 2.0, mmax);

for i = 1:mmax
  for j = 1:mmax

    x(1) = x0(j);
    y(1) = y0(i);

    for n = 1:nmax - 1
      x(n + 1) = x(n)^2 - y(n)^2 + a*x(n) + b*y(n);
      y(n + 1) = 2*x(n)*y(n) + c*x(n) + d*y(n);
    endfor

    if sqrt(x(n)^2 + y(n)^2) < 5.0
      M(i, j) = 1;
    endif

  endfor
endfor

contourf(x0, y0, M)
axis equal
colormap bone
title('Basin Boundary of the Tinkerbell Map')
xlabel('x')
ylabel('y')
