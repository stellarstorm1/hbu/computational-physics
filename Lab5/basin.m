% Lab 5 - Basin Boundary of the Henon map
clear all
nmax  = 50;
x=zeros(nmax,1);
y=zeros(nmax,1);

a=1.4; b=0.3;

mmax = 300;

M = zeros(mmax,mmax);

x0 = linspace(-2.0,2.0,mmax);
y0 = linspace(-2.0,2.0,mmax);

for i = 1:mmax
  for j = 1:mmax
    x(1) = x0(j);
    y(1) = y0(i);

    for n = 1:nmax - 1
      y(n + 1) = b * x(n);
      x(n + 1) = 1 - a * x(n)^2 + y(n);
    endfor

    if sqrt(x(n)^2 + y(n)^2) < 5.0
      M(i, j) = 1;
    else
      M(i, j) = 0;
    endif

  endfor
endfor
contourf(x0,y0,M)
hold on

nmax = 500;
 x(1)=1.2;
 y(1)=0.0;

for n=1:nmax-1
  y(n+1) = b*x(n);
  x(n+1) = 1-a*x(n)^2 + y(n);
end

plot(x,y,'ok')
axis equal
colormap bone
title('Basin Boundary of the Henon Map')
xlabel('x')
ylabel('y')
