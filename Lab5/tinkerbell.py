"""Lab 5 - Contour and Basin boundary of the Tinkerbell map"""
import matplotlib.pyplot as plt
import numpy as np

nmax = 10  # 50 in assignment but overlow error
x = np.zeros(nmax)
y = np.zeros(nmax)

a = 0.9
b = -0.6013
c = 2.0
d = 0.50

mmax = 300

M = np.zeros(shape=(mmax, mmax))

x0 = np.linspace(-2.0, 2.0, mmax)
y0 = np.linspace(-2.0, 2.0, mmax)

for i in range(mmax):
    for j in range(mmax):
        x[0] = x0[j]
        y[0] = y0[i]

        for n in range(nmax - 1):
            x[n + 1] = x[n]**2 - y[n]**2 + a * x[n] + b * y[n]
            y[n + 1] = 2 * x[n] * y[n] + c * x[n] + d * y[n]

        if np.sqrt(x[n]**2 + y[n]**2) < 5.0:
            M[i, j] = 1
plt.contourf(x0, y0, M, cmap='gray')
plt.title('Basin boundary of the Tinkerbell Map')
plt.xlabel('x')
plt.ylabel('y')
plt.show()
