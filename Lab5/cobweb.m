% Lab 5 - Cobweb plot of tent map
clear all
mu = 1.6;
x0 = 0.2;
Nmax = 21;

x = zeros(Nmax, 1);
t = zeros(Nmax, 1);
y = zeros(Nmax, 1);

x(1) = x0;

for i=1:Nmax
  if x(i) < 0.5 && x(i) > 0
    x(i+1) = mu*x(i);
  elseif x(i) < 1.0 && x(i) >= 0.5
    x(i + 1) = mu * (1 - x(i));
  endif
endfor

for i = 1:Nmax
  t(i) = (i - 1)/(Nmax - 1);
  if t(i) < 0.5 && t(i) > 0
    y(i) = mu*t(i);
  elseif t(i) < 1.0 && t(i) >= 0.5
    y(i) = mu * (1 - t(i));
  endif
endfor

hold on

plot(t, t, t, y)

line([x(2) x(1)], [0 x(2)])

for i=1:Nmax - 1
  line([x(i) x(i + 1)], [x(i + 1) x(i + 1)]);
  line([x(i+1) x(i+1)], [x(i+1) x(i+2)]);
endfor

hold off
axis equal
title('Cobweb plot of the tent map')
xlabel('x(n)')
ylabel('y(n+1)')

colormap bone
