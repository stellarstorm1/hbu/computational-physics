"""Lab 5 - Basin map of Julia set"""
import matplotlib.pyplot as plt
import numpy as np

nmax = 10
x = np.zeros(nmax)
y = np.zeros(nmax)

mmax = 100

J = np.zeros(shape=(mmax, mmax))

x0 = np.linspace(-1.5, 1.5, mmax)
y0 = np.linspace(-1.5, 1.5, mmax)

const = np.complex(-0.75, 0.02)

for i in range(mmax - 1):
    for j in range(mmax - 1):
        x[0] = x0[i]
        y[0] = y0[j]

        for n in range(nmax - 2):
            y[n + 1] = 2.0 * x[n] * y[n] + const.imag
            x[n + 1] = x[n]**2 - y[n]**2 + const.real
        if np.sqrt(x[n]**2 + y[n]**2) < 5.0:
            J[i, j] = 1
plt.contourf(J, cmap='gray')
plt.title('Julia set')
plt.show()
