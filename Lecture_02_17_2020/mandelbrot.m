clear all
nmax  = 50;
x=zeros(nmax,1);
y=zeros(nmax,1);

mmax = 100;  % 500 in original

M = zeros(mmax,mmax);

x0 = linspace(-1.5,.5,mmax);
y0 = linspace(-1.0,1.0,mmax);

for i = 1:mmax
  for j = 1:mmax
          
    for n=1:nmax-1
      y(n+1) = 2*x(n)*y(n) + y0(i);
      x(n+1) = x(n)^2 - y(n)^2 + x0(j);
    end

    if sqrt(x(n)^2 + y(n)^2) < 2.0
      M(i,j)= 1;
    end
  end
end
contourf(M)

