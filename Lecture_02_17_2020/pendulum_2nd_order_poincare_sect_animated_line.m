q=4;
g=2.5;
wD=.7;
dt=.01;

nmax= 100000000; 


omega=.1; theta=.1; phi=.1;

hh =animatedline;
hh.LineStyle='none';
hh.Marker='.';

% Look Ma no indexing !

for n=1:nmax

F1 = -omega/q - sin(theta)+g*cos(phi);
F2 = omega;
F3 = wD;

F1b=-(omega+F1*dt)/q - sin(theta)+g*cos(phi);

phi1=phi;

omega = omega + F1b*dt;
theta = theta + F2*dt;
phi = phi+ F3*dt;

% periodic boundary conditions

if theta > pi
   theta = theta -2*pi;
elseif theta < -pi
    theta = theta + 2*pi;
end

% poincare section 

if cos(phi1)/cos(phi)<0 
  addpoints(hh,theta,omega)
  drawnow  
end


end

xlabel('omega')
ylabel('theta')
