clear all
nmax  = 50;
x=zeros(nmax,1);
y=zeros(nmax,1);

a=1.4; b=0.3;

mmax = 100;  % 500

M = zeros(mmax,mmax);

x0 = linspace(-3.0,3.0,mmax);
y0 = linspace(-3.0,3.0,mmax);

for i = 1:mmax
  for j = 1:mmax   
  x(1)=x0(j);
  y(1)=y0(i);

  for n=1:nmax-1
    y(n+1) = b*x(n);
    x(n+1) = 1-a*x(n)^2 + y(n);
  end

  if sqrt(x(n)^2 + y(n)^2) < 5.0
    M(i,j)= 1;
    else
    M(i,j)=0; 
  end
  end
end
contourf(x0,y0,M)
hold on 

nmax = 10000;
x(1)=0;
y(1)=0;
 
for n=1:nmax-1
  y(n+1) = b*x(n);
  x(n+1) = 1-a*x(n)^2 + y(n);
end
colormap gray
plot(x,y,'ok')