% Lorenz Model Lab -- tweaked

rho = 28;
sigma = 10;
beta = 8/3;
dt = .01;

f = @(x, y, z)sigma * (y - x);
g = @(x, y, z)rho * x - y - x * z;
h = @(x, y, z)x * y - beta * z;

nmax = 2^12;

x = zeros(nmax, 1);
y = zeros(nmax, 1);
z = zeros(nmax, 1);

x(1) = .1;
y(1) = .1;
z(1) = .1;

for n = 1:nmax
    x(n + 1) = x(n) + f(x(n), y(n), z(n)) * dt;
    y(n + 1) = y(n) + g(x(n), y(n), z(n)) * dt;
    z(n + 1) = z(n) + h(x(n), y(n), z(n)) * dt;
endfor

plot(x, y, 'k')
xlabel('x')
ylabel('y')
