#!/usr/bin/env python3
"""Lorenz Model Lab - Runge-Kutta method"""

import numpy as np
import matplotlib.pyplot as plt

dt = 0.01
nmax = 10000


def f(x, y):
    return -x - (2 * x * y)


def g(x, y):
    return -y - x**2 + y**2


def h(px, py):
    return px


def w(px, py):
    return py


x = np.zeros(nmax)
y = np.zeros(nmax)
px = np.zeros(nmax)
py = np.zeros(nmax)

x[0] = 0.1
y[0] = 0.1
px[0] = 0.1
py[0] = 0.1

for i in range(nmax - 1):
    kx1 = dt * h(px[i], py[i])
    kx2 = dt * h(px[i] + kx1 / 2, py[i])
    kx3 = dt * h(px[i] + kx2 / 2, py[i])
    kx4 = dt * h(px[i] + kx3, py[i])
    x[i + 1] = x[i] + (kx1 + 2 * kx2 + 2 * kx3 + kx4) / 6

    ky1 = dt * w(px[i], py[i])
    ky2 = dt * w(px[i], py[i] + ky1 / 2)
    ky3 = dt * w(px[i], py[i] + ky2 / 2)
    ky4 = dt * w(px[i], py[i] + ky3)
    y[i + 1] = y[i] + (ky1 + 2 * ky2 + 2 * ky3 + ky4) / 6

    kpx1 = dt * f(x[i], y[i])
    kpx2 = dt * f(x[i] + kpx1 / 2, y[i])
    kpx3 = dt * f(x[i] + kpx2 / 2, y[i])
    kpx4 = dt * f(x[i] + kpx3, y[i])
    px[i + 1] = px[i] + (kpx1 + 2 * kpx2 + 2 * kpx3 + kpx4) / 6

    kpy1 = dt * g(x[i], y[i])
    kpy2 = dt * g(x[i], y[i] + kpy1 / 2)
    kpy3 = dt * g(x[i], y[i] + kpy2 / 2)
    kpy4 = dt * g(x[i], y[i] + kpy3)
    py[i + 1] = py[i] + (kpy1 + 2 * kpy2 + 2 * kpy3 + kpy4) / 6

plt.plot(x, y)
plt.show()
