#!/usr/bin/env python3
"""Lorenz Model Lab - Runge-Kutta method"""

import numpy as np
import matplotlib.pyplot as plt

rho = 28
sigma = 10
beta = 8 / 3
dt = 0.01

nmax = 10000


def f(x, y, z):
    return sigma * (y - x)


def g(x, y, z):
    return rho * x - y - x * z


def h(x, y, z):
    return x * y - beta * z


x = np.zeros(nmax)
y = np.zeros(nmax)
z = np.zeros(nmax)

x[0] = 0.1
y[0] = 0.1
z[0] = 0.1

for i in range(nmax - 1):
    kx1 = dt * f(x[i], y[i], z[i])
    kx2 = dt * f(x[i] + kx1 / 2, y[i], z[i])
    kx3 = dt * f(x[i] + kx2 / 2, y[i], z[i])
    kx4 = dt * f(x[i] + kx3, y[i], z[i])
    x[i + 1] = x[i] + (kx1 + 2 * kx2 + 2 * kx3 + kx4) / 6

    ky1 = dt * g(x[i], y[i], z[i])
    ky2 = dt * g(x[i], y[i] + ky1 / 2, z[i])
    ky3 = dt * g(x[i], y[i] + ky2 / 2, z[i])
    ky4 = dt * g(x[i], y[i] + ky3, z[i])
    y[i + 1] = y[i] + (ky1 + 2 * ky2 + 2 * ky3 + ky4) / 6

    kz1 = dt * h(x[i], y[i], z[i])
    kz2 = dt * h(x[i], y[i], z[i] + kz1 / 2)
    kz3 = dt * h(x[i], y[i], z[i] + kz2 / 2)
    kz4 = dt * h(x[i], y[i], z[i] + kz3)
    z[i + 1] = z[i] + (kz1 + 2 * kz2 + 2 * kz3 + kz4) / 6

plt.plot(x, y)
plt.show()
