% Lorenz Model Lab -- Modified Euler method

rho = 28;
sigma = 10;
beta = 8/3;
dt = .01;

f = @(x, y, z)sigma * (y - x);
g = @(x, y, z)rho * x - y - x * z;
h = @(x, y, z)x * y - beta * z;

nmax = 10000;

x = zeros(nmax, 1);
y = zeros(nmax, 1);
z = zeros(nmax, 1);

x(1) = .1;
y(1) = .1;
y(1) = .1;

for n = 1:nmax
    kx1 = f(x(n), y(n), z(n)) * dt;
    kx2 = f(x(n) + kx1 / 2, y(n), z(n)) * dt;
    x(n + 1) = x(n) + kx1;

    ky1 = g(x(n), y(n), z(n)) * dt;
    ky2 = g(x(n), y(n) + ky1 / 2, z(n)) * dt;
    y(n + 1) = y(n) + ky2;

    kz1 = h(x(n), y(n), z(n)) * dt;
    kz2 = h(x(n), y(n), z(n) + kz1 / 2) * dt;
    z(n + 1) = z(n) + kz2;
endfor

plot(x, y)
xlabel('x')
ylabel('y')
