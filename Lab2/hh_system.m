% Lorenz Model Lab -- Henon-Heiles system solved with Runge-Kutta method

dt = .01;

f = @(x, y) - x - 2 * x * y;
g = @(x, y) - y - x^2 + y^2;
h = @(px, py)px;
w = @(px, py)py;

nmax = 10000;

x = zeros(nmax, 1);
y = zeros(nmax, 1);
px = zeros(nmax, 1);
py = zeros(nmax, 1);

x(1) = .1;
y(1) = .1;
px(1) = .1;
py(1) = .1;

for n = 1:nmax
    kx1 = dt * h(px(n), py(n));
    kx2 = dt * h(px(n) + kx1 / 2, py(n));
    kx3 = dt * h(px(n) + kx2 / 2, py(n));
    kx4 = dt * h(px(n) + kx3, py(n));
    x(n + 1) = x(n) + (kx1 + 2 * kx2 + 2 * kx3 + kx4) / 6;

    ky1 = dt * w(px(n), py(n));
    ky2 = dt * w(px(n), py(n) + ky1 / 2);
    ky3 = dt * w(px(n), py(n) + ky2 / 2);
    ky4 = dt * w(px(n), py(n) + ky3);
    y(n + 1) = y(n) + (ky1 + 2 * ky2 + 2 * ky3 + ky4) / 6;

    kpx1 = dt * f(x(n), y(n));
    kpx2 = dt * f(x(n) + kpx1 / 2, y(n));
    kpx3 = dt * f(x(n) + kpx2 / 2, y(n));
    kpx4 = dt * f(x(n) + kpx3, y(n));
    px(n + 1) = px(n) + (kpx1 + 2 * kpx2 + 2 * kpx3 + kpx4) / 6;

    kpy1 = dt * g(x(n), y(n));
    kpy2 = dt * g(x(n), y(n) + kpy1 / 2);
    kpy3 = dt * g(x(n), y(n) + kpy2 / 2);
    kpy4 = dt * g(x(n), y(n) + kpy3);
    py(n + 1) = py(n) + (kpy1 + 2 * kpy2 + 2 * kpy3 + kpy4) / 6;

endfor

plot(x, y)
xlabel('x')
ylabel('y')
