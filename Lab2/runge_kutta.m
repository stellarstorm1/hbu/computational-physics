% Lorenz Model Lab -- Runge-Kutta method

rho = 28;
sigma = 10;
beta = 8/3;
dt = .01;

f = @(x, y, z)sigma * (y - x);
g = @(x, y, z)rho * x - y - x * z;
h = @(x, y, z)x * y - beta * z;

nmax = 10000;

x = zeros(nmax, 1);
y = zeros(nmax, 1);
z = zeros(nmax, 1);

x(1) = .1;
y(1) = .1;
z(1) = .1;

for n = 1:nmax
    kx1 = f(x(n), y(n), z(n)) * dt;
    kx2 = f(x(n) + kx1 / 2, y(n), z(n)) * dt;
    kx3 = f(x(n) + kx2 / 2, y(n), z(n)) * dt;
    kx4 = f(x(n) + kx3, y(n), z(n)) * dt;
    x(n + 1) = x(n) + (kx1 + 2 * kx2 + 2 * kx3 + kx4) / 6;

    ky1 = g(x(n), y(n), z(n)) * dt;
    ky2 = g(x(n), y(n) + ky1 / 2, z(n)) * dt;
    ky3 = g(x(n), y(n) + ky2 / 2, z(n)) * dt;
    ky4 = g(x(n), y(n) + ky3, z(n)) * dt;
    y(n + 1) = y(n) + (ky1 + 2 * ky2 + 2 * ky3 + ky4) / 6;

    kz1 = h(x(n), y(n), z(n)) * dt;
    kz2 = h(x(n), y(n), z(n) + kz1 / 2) * dt;
    kz3 = h(x(n), y(n), z(n) + kz2 / 2) * dt;
    kz4 = h(x(n), y(n), z(n) + kz3) * dt;
    z(n + 1) = z(n) + (kz1 + 2 * kz2 + 2 * kz3 + kz4) / 6;
endfor

plot(x, y)
xlabel('x')
ylabel('y')
