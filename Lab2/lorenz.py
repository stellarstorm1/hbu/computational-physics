#!/usr/bin/env python3
"""Lorenz Model Lab"""

import numpy as np
import matplotlib.pyplot as plt

rho = 28
sigma = 10
beta = 8 / 3
dt = 0.01

nmax = 10000

x = np.zeros(nmax)
y = np.zeros(nmax)
z = np.zeros(nmax)

x[0] = 0.1
y[0] = 0.1
z[0] = 0.1

for i in range(nmax - 1):
    x[i + 1] = x[i] + sigma * (y[i] - x[i]) * dt
    y[i + 1] = y[i] + (rho * x[i] - y[i] - x[i] * z[i]) * dt
    z[i + 1] = z[i] + (x[i] * y[i] - beta * z[i]) * dt

plt.plot(x, y)
plt.show()
