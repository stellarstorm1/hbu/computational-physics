% Lorenz Model Lab

rho = 28;
sigma = 10;
beta = 8/3;
dt = .01;

nmax = 10000;

x = zeros(nmax, 1);
y = zeros(nmax, 1);
z = zeros(nmax, 1);

x(1) = .1;
y(1) = .1;
z(1) = .1;

for n = 1:nmax
    x(n + 1) = x(n) + sigma * (y(n) - x(n)) * dt;
    y(n + 1) = y(n) + (rho * x(n) - y(n) - x(n) * z(n)) * dt;
    z(n + 1) = z(n) + (x(n) * y(n) - beta * z(n)) * dt;
endfor

plot3(x, y, z)
xlabel('x')
ylabel('y')
