% graphics_toolkit('gnuplot');
% graphics_toolkit('fltk');
% graphics_toolkit('qt');

h = animatedline;
axis([0,4*pi,-1,1])
x = linspace(0,4*pi,1000);
y = sin(x);
for k = 1:length(x)
  addpoints(h,x(k),y(k));
  drawnow
end
fprintf('<Enter> to continue ...\n');
pause
close('all');

x = [1 2];
y = [1 2];
h = animatedline(x,y,'Color','r','LineWidth',3);
fprintf('<Enter> to continue ...\n');
pause
close('all');

h = animatedline('MaximumNumPoints',100);
axis([0,4*pi,-1,1])
x = linspace(0,4*pi,1000);
y = sin(x);
for k = 1:length(x)
  addpoints(h,x(k),y(k));
  drawnow
end
fprintf('<Enter> to continue ...\n');
pause
close('all');

h = animatedline;
axis([0,4*pi,-1,1])
numpoints = 100000;
x = linspace(0,4*pi,numpoints);
y = sin(x);
for k = 1:100:numpoints-99
  xvec = x(k:k+99);
  yvec = y(k:k+99);
  addpoints(h,xvec,yvec)
  drawnow
end
fprintf('<Enter> to continue ...\n');
pause
close('all');

h = animatedline;
axis([0,4*pi,-1,1])
numpoints = 10000;
x = linspace(0,4*pi,numpoints);
y = sin(x);
a = tic; % start timer
for k = 1:numpoints
  addpoints(h,x(k),y(k))
  b = toc(a); % check timer
  if b > (1/30)
     drawnow % update screen every 1/30 seconds
     a = tic; % reset timer after updating
 end
end
drawnow % draw final frame
fprintf('<Enter> to continue ...\n');
pause
close('all');

ah = animatedline(1:10, (1:10).^2, 'color', 'r', 'marker', 'v');
fprintf('<Enter> to continue ...\n');
pause
ah.color = 'g';
fprintf('<Enter> to continue ...\n');
pause
close('all');

z = [0:0.05:5];
x = cos (2*pi*z);
y = sin (2*pi*z);
ah = animatedline(x, y, z);
fprintf('<Enter> to continue ...\n');
pause
close('all');
