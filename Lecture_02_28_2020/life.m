% Lecture 02/28/2020 - simple Conway's Game of Life
clear all
N = 75;

% Create an array of 1s and 0s randomly distributed
R = randn(N);
A = zeros(N);
Anew = zeros(N);
A(find(R > 1)) = 1;

for k = 1:200
  for i = 2:N - 1
    for j = 2:N - 1
      sum = A(i - 1, j + 1) + A(i, j + 1) + A(i + 1, j + 1) + A(i + 1, j) + ...
      A(i - 1, j) + A(i - 1, j - 1) + A(i, j - 1) + A(i + 1, j - 1);

      if sum < 2 && A(i, j) == 1
        Anew(i, j) = 0;
      elseif sum > 3 && A(i, j) == 1
        Anew(i, j) = 0;
      elseif sum == 3 && A(i, j) == 0
        Anew(i, j) = 1;
      endif
    endfor
  endfor
  imagesc(Anew);
  pause(0.01)
  A = Anew;
endfor
