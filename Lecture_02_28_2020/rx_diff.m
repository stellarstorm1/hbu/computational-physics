% Lecture 02/28/2020 - Reaction-Diffusion
clear all

Nmax = 200;
n = 100;
D = 0.2;
gamma = 0.5;

reaction = 'u.*(1 - u)';

% initialize u and grad arrays
u = rand(n);
grad = zeros(n);

i = 2:n-1;
j = 2:n-1;

for k = 1:Nmax
  grad(i, j) = u(i, j-1) + u(i, j+1) + u(i-1, j) + u(i+1, j);
  u = (1- 4*D) * u + D*grad + gamma*eval(reaction);
endfor

colormap bone
pcolor(u);
shading interp;
