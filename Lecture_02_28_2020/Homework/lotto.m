% Lecture 02/28/2020 homework - Lottery simulation
% Done in Lab 6
clear all
winning = sort(randperm(69, 5));
draws = 1;
bought = sort(randperm(69, 5));
while isequal(winning, bought) == 0
  bought = sort(randperm(69, 5));
  draws = draws + 1;
endwhile
fprintf("It took %d draws to win\n", draws);