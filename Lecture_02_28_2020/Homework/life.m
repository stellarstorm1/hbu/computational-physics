% Homework from 02/28/2020 - simple Conway's Game of Life with periodic
% boundary conditions
clear all
N = 75;

% Create an array of 1s and 0s randomly distributed
R = randn(N);
A = zeros(N);
Anew = zeros(N);
A(find(R > 1)) = 1;

while 0 == 0
  for i = 1:N
    for j = 1:N
      if i == 1
        i_left = N;
        i_right = i + 1;
      elseif i == N
        i_left = i - 1;
        i_right == 1;
      else
        i_left = i - 1;
        i_right = i + 1;
      endif
      
      if j == 1
        j_below = j + 1;
        j_above = N;
      elseif j == N
        j_below = 1;
        j_above = j - 1;
      else
        j_below = j + 1;
        j_above = j - 1;
      endif
      
      sum = A(i_left, j_below) + A(i, j_below) + A(i_right, j_below) + ...
      A(i_right, j) + A(i_left, j) + A(i_left, j_above) + A(i, j_above) +...
      A(i_right, j_above);

      if sum < 2 && A(i, j) == 1
        Anew(i, j) = 0;
      elseif sum > 3 && A(i, j) == 1
        Anew(i, j) = 0;
      elseif sum == 3 && A(i, j) == 0
        Anew(i, j) = 1;
      endif
    endfor
  endfor
  imagesc(Anew);
  pause(0.01)
  A = Anew;
endwhile