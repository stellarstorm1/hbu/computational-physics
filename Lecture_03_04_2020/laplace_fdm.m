% Lecture 03/02/2020 - Laplace's Equation solved with Finite Difference Method
clear all
% set the number of grid points and iteration number
imax = 20;
jmax = 20;
nmax = 1000;

% initialize the grid
S = zeros(imax, jmax);

% specify the boundary conditions: S=1 on one side, 0 on other
S(:, jmax) = 1;

% perform the finite difference calculation
for n = 1:nmax
  for i = 2:imax - 1
    for j = 2:jmax - 1
      S(i, j) = (S(i + 1, j) + S(i - 1, j) + S(i, j + 1) + S(i, j - 1)) / 4;
    endfor
  endfor
endfor

% create a surface plot of the scalar field
mesh(S)