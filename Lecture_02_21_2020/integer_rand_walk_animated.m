n_steps = 10000;

i = 0;
j = 0;

hh = animatedline;

for k=1:n_steps
  z=rand;
  
  if z < 0.25
    i = i - 1;
   elseif 0.25 <= z && z < 0.5;
    j = j - 1;
   elseif 0.5 <= z && z < 0.75
    i = i + 1;
  elseif z >= 0.75
    j = j + 1;
  endif
  addpoints(hh, i, j)
  drawnow
endfor
