clear all
n_steps = 10000;
N = 200;

A = zeros(N);
A(N/2, N/2) = 1;

for k=1:n_steps
  % initial particles
  i = int16((N - 2)*rand) + 2;
  j = int16((N - 2)*rand) + 2;
  % Avoid indexing outside the array
  while i > N
    i = int16((N - 2)*rand) + 2;
  endwhile
  while j > N
    j = int16((N - 2)*rand) + 2;
  endwhile
  
  while A(i, j) == 0
    z = rand;
    if z < 0.25
      i = i - 1;
    elseif 0.25 <= z && z < 0.5;
      j = j - 1;
    elseif 0.5 <= z && z < 0.75
      i = i + 1;
    elseif z >= 0.75
      j = j + 1;
    endif
    
    % apply periodic boundary conditions
    if i == 1
      i = N - 1;
    endif
    if j == 1
      j = N - 1;
    endif
    
    if i == N
      i = 2;
    endif
    if j == N
      j = 2;
    endif
    
    if A(i - 1, j) == 1 || A (i + 1, j) == 1 || A(i, j + 1) == 1 ||...
        A(i, j - 1) == 1 || A(i + 1, j + 1) == 1 || A(i - 1, j - 1) == 1 ||...
        A(i - 1, j + 1) == 1 || A(i + 1, j - 1) == 1
      A(i, j) = 1;
    endif
  endwhile
  % can move this outside the loop to speed things up
  imagesc(A);
  % needed so the graph updates/rescales properly
  pause(0.000001);
endfor