% Homework from Lecture 02/21/2020 -  3D Random Walk

n_steps=10000;

i = 0;
j = 0;
k = 0;

figure

for n=1:n_steps
  i = i + randn;
  j = j + randn;
  k = k + randn;
  plot3(i, j, k, 'ok')
  hold on
  drawnow
endfor
