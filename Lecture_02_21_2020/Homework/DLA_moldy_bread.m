% Homework from Lecture 02/21/2020 - random walk simulation of molding bread
n_steps = 10000;
N = 200;

A = zeros(N);

% add "crust"
A(1:N, 1) = 1;
A(1:N, N) = 1;
A(1, 1:N) = 1;
A(N, 1:N) = 1;

% Pick a random value on the crust for the mold to start
if randi([0, 1]) == 0
  top_bottom_position = 1;
else
  top_bottom_position = N;
endif
if randi([0, 1]) == 0
  left_right_position = 1;
else
  left_right_position = N;
endif

A(top_bottom_position, left_right_position) = 1;

for k=1:n_steps
  % initial particles
  i = int16((N - 2)*rand) + 2;
  j = int16((N - 2)*rand) + 2;
  % Avoid indexing outside the array
  while i > N
    i = int16((N - 2)*rand) + 2;
  endwhile
  while j > N
    j = int16((N - 2)*rand) + 2;
  endwhile
  
  while A(i, j) == 0
    z = rand;
    if z < 0.25
      i = i - 1;
    elseif 0.25 <= z && z < 0.5;
      j = j - 1;
    elseif 0.5 <= z && z < 0.75
      i = i + 1;
    elseif z >= 0.75
      j = j + 1;
    endif

    % avoid testing out of bounds
    if i == 1
      i_boundary = i + 1;
    elseif i == N
      i_boundary = i - 1;
     else
      i_boundary = i;
    endif
    if j == 1
      j_boundary = j + 1;
    elseif j == N
      j_boundary = j - 1;
    else
      j_boundary = j;
    endif
    
    if A(i_boundary - 1, j) == 1 || A (i_boundary + 1, j) == 1 ||...
        A(i, j_boundary + 1) == 1 || A(i, j_boundary - 1) == 1 ||...
        A(i_boundary + 1, j_boundary + 1) == 1 ||...
        A(i_boundary - 1, j_boundary - 1) == 1 ||...
        A(i_boundary - 1, j_boundary + 1) == 1 ||...
        A(i_boundary + 1, j_boundary - 1) == 1
      A(i, j) = 1;
    endif
  endwhile
  % can move this outside the loop to speed things up
  imagesc(A);
  % needed so the graph updates/rescales properly
  pause(0.000001);
endfor