n_steps=10000;

i = 0;
j = 0;

figure
hold on

for k=1:n_steps
  i = i + randn;
  j = j + randn;
  plot(i, j, '.k')
endfor
